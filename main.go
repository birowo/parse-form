// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
)

type Rdr struct {
	bs []byte
	n  int
}

func (rdr *Rdr) Read(bs []byte) (int, error) {
	if rdr.n < len(rdr.bs) {
		n := copy(bs, rdr.bs[rdr.n:])
		rdr.n += n
		return n, nil
	}
	return 0, io.EOF

}
func ParseForm(r *http.Request, bfr []byte) error { //tanpa cek method & enctype
	n, err := r.Body.Read(bfr)
	if err == nil || err == io.EOF {
		r.Form, err = url.ParseQuery(string(bfr[:n]))
	}
	return err
}
func main() {
	fmt.Println("Hello, 世界")

	srvr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch r.RequestURI {
		case "/1":
			ParseForm(r, make([]byte, 16))
			fmt.Println("/1 form:", r.Form)
		case "/2":
			r.ParseForm()
			fmt.Println("/2 form:", r.Form, ",post form", r.PostForm)
		}
	}))
	rdr := &Rdr{[]byte("a=b&a=c"), 0}
	rqst, err := http.NewRequest("", srvr.URL+"/1", rdr)
	if err != nil {
		println(err.Error())
		return
	}
	(&http.Client{}).Do(rqst)
	rdr.n = 0
	http.Post(srvr.URL+"/2", "application/x-www-form-urlencoded", rdr)
}
